﻿using ChangeMakers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Mail;
using System.Net;
using ChangeMakers.Controllers;
using System.Runtime.CompilerServices;

namespace ChangeMakers.Controllers
{
    public class HomeController : Controller
    { 
        ChangeMakerEntities CME = new ChangeMakerEntities();
        ChangeMakerDBEntities CMDB = new ChangeMakerDBEntities();
        // GET: Home
        public ActionResult Index()
        {
            if(Session["MemberId"] == null || Session["SessionId"] == null)
            {
                return RedirectToAction("Login","Home");
            }
            else
            { 
                int memberid=Convert.ToInt32(Session["MemberId"]);
                ViewBag.UserName = ChangeMakerEntities.UsrFName;
                ViewBag.UserLName = ChangeMakerEntities.UsrLName;
                ViewBag.MemberId = memberid;
                ViewBag.TotalTokens = TotalTokens(ChangeMakerEntities.LevelID);
                ViewBag.ActiveMembers = CountActiveMember(memberid);
                ViewBag.AuthRefCode = ChangeMakerEntities.AuthRefByCode;
                ViewBag.AuthRefName = ChangeMakerEntities.AuthRefName;
                ViewBag.MyRefCode = ChangeMakerEntities.MyRefCode;
                var MyMember = CME.GetMyMember(memberid);
                ViewBag.UserProfilePic = CME.GetProfilePic(memberid);
                //  if (MyMember != null) { ViewBag.MyMember = MyMember; }
                ViewBag.MyMember = MyMember;
                ViewBag.TotalReg = countTotalReg();
                if (ChangeMakerEntities.TotalKit <= 24) { ViewBag.RequiredKit = 25 - ChangeMakerEntities.TotalKit; }
                return View();
            }
        }
        public int countTotalReg()
        {
            int a = CMDB.tblMembers.Count();
            return a;
        }
        public int CountActiveMember(int id)
        {
            int Count = 0;
            try
            {
              Count = CMDB.tblReferralCodes.Count(x => x.MemberId == id);
            }
            catch (Exception) { }
            return Count;
        }
        public int TotalTokens(int Lvlid)
        {
            int TotalKits = ChangeMakerEntities.TotalKit;
            int totaltokens = 0;
            totaltokens = TotalKits * 1500;
            return totaltokens;
        }
        public ActionResult Index1()
        {

            var a= "<script language='javascript' type='text/javascript'>alert('Thanks for Feedback!');</script>";
           // string message1 = "";
           // string ActivationUrl = "";
           //// string message2 = "Invalid data";
           // try
           // {
           //     string email = "anand.ap164@gmail.com";
           //    // string verificationCode = "123456";
           //     ActivationUrl = Server.HtmlEncode("http://localhost:54165/Home/Verification?email="+email+"&key=" + GetKey(email));
           //     WebMail.Send(email,
           //                "TMK Technology-Verify Your Email",
           //               "Welcome  <br/>" + "<a href='" + ActivationUrl + "'>Click Here to verify your acount</a>" + "<br/>Thanks, <br/><b>Team MeAplpha</b>",
           //               null, null, null,
           //               true,
           //               null, null, null, null, null,
           //               email
           //               );

           // }
           // catch (Exception ex)
           // {

           //     message1 = ex.Message.ToString();
           //     ViewBag.message1 = message1;
           //     return View();
           // }
           // message1 = "success";
            ViewBag.message1 = a;
            return View("<script language='javascript' type='text/javascript'>alert('Thanks for Feedback!');</script>");
        }
        public static string EFName = "";
        private string GetKey(string email)
        {
            var user = CMDB.tblMembers.FirstOrDefault(x => x.Email == email);
            if(user!=null)
            {
                var user1 = CMDB.tblKits.FirstOrDefault(x => x.MemberId == user.MemberId);
                if (user1 != null)
                {
                    EFName = user.FirstName;
                    return user1.Key;
                }
            }
            return "";
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Login", "User");
        }
        public ActionResult Login()
        {
            //return RedirectToAction("Login", "User");
            if (Session["MemberId"] != null || Session["SessionId"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                string message = "";
                ViewBag.LoginMessage = message;
                return View();
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel logininfo)
        {
            if (Session["MemberId"] != null || Session["SessionId"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                string InvalidDetailsMSG = "Invalid Login Details";
                string InActiveMSG = "You have not verified your email during registration !! ";
                if (ModelState.IsValid)
                {
                    bool IsValid = CME.LoginInfo(logininfo);
                    if (IsValid)
                    {
                        int memberid =Convert.ToInt32( Session["MemberId"]);
                        if(CME.IsActive(memberid))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        else {
                            Session.RemoveAll();
                        ViewBag.VerifyMsg = InActiveMSG;
                        }
                    }
                    else
                        ViewBag.LoginMessage = InvalidDetailsMSG;
                }
                return View();
            }
            
        }
        public ActionResult Registration()
        {
            ViewBag.Message = "";
            return View();
        }
        
        public static string OTP = "";
        [HttpPost]
        public ActionResult Registration(MemberViewModel mvm)
        {
            UserController.IsVerification = true;
            //Random rd = new Random();
            //OTP = rd.Next(Convert.ToInt32(100000), Convert.ToInt32(999999)).ToString();
            //string VerificatioCode = "CMW - " + OTP;
            var errors = ModelState.Where(x => x.Value.Errors.Any())
                .Select(x => new { x.Key, x.Value.Errors });
            if (ModelState.IsValid)
            {
                bool IsValid = false;
                if(IsValid=CME.IsValidUser(mvm.UserName))
                {
                    if (IsValid = CME.IsEmailExist(mvm.Email))
                    {
                        if (IsValid = CME.IsValidRID(mvm.ReferralCode))
                        {
                            if (IsValid = CME.InsertInMember(mvm))
                            {
                                IsValid = SendVerificationEmail(mvm.Email);
                                // return RedirectToAction("Login", "Home");
                                return Redirect("~/HtmlPages/Activate.html");
                            }
                            else
                            {
                                ViewBag.Message = "There is Problem during registration.!";
                                return View();
                            }
                        }
                        else
                        {
                            ViewBag.Message = "Invalid Referance Id";
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.Message = "Email Already Exist";
                        return View();
                    }
                }
                else
                {
                    ViewBag.Message = "User Name Already Exist";
                    return View();
                }
            }
            ViewBag.Message = "";
            return View();
        }

        
        [MethodImpl(MethodImplOptions.Synchronized)]
        private bool SendVerificationEmail(string email)
        {
            string ActivationUrl = "";
            bool Astatus = true;
          //  ActivationUrl = Server.HtmlEncode("http://localhost:54165/Home/Verification?email=" + email + "&key=" + GetKey(email) + "&Astatus=" + Astatus);
              ActivationUrl = Server.HtmlEncode("https://portal.changemakersworld.com/Home/Verification?email=" + email + "&key=" + GetKey(email) + "&Astatus=" + Astatus);
            string MessageBodyAtivate = "<html><body><table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='height:100%;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;padding:0;width:100%;'> <tbody> <tr> <td align='center' valign='top' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;background-color:#f7f7f7;height:100%;mso-line-height-rule:exactly;padding:0;width:100%;'> <table width='100%' cellpadding='0' cellspacing='0' border='0' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;'> <tbody> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#f7f7f7;mso-line-height-rule:exactly;' align='center' valign='top'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:640px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr>" +
            " <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-bottom:17px;padding-left:20px;padding-right:20px;padding-top:20px;text-align:left;'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:530px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr> <td width='50%' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-right:20px;'> <img src='https://image.ibb.co/nsFYA8/favicon_32x32.png' style=' margin-bottom:-10px;' /><label style='margin-left:5px;margine-top=-10px;'><b>Changemakers World</b></label> </td> <td width='50%' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;'> <table align='right' cellpadding='0' cellspacing='0' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;'> <tbody> <tr> <td width='20' " + " style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-right:10px;'> <img src='https://gallery.mailchimp.com/4af5a9e2e2d51c22c87e8d44a/images/7d634ffd-2892-4e6d-9ba4-3ee5e8813155.png' " +
            " width='21' height='19' alt='' style='-ms-interpolation-mode:bicubic'> </td> <td style='-ms-text-size-adjust:100%;-webkit-font-smoothing:subpixel-antialiased;-webkit-text-size-adjust:100%;color:#2a2c39;font-family:' Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif';font-size:12px;line-height:18px;mso-line-height-rule:exactly;text-align:left;padding-bottom:3px;font-weight:700;white-space:nowrap;'>" + EFName + "</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#f7f7f7;mso-line-height-rule:exactly;' align='center' valign='top'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:600px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#fff;mso-line-height-rule:exactly;padding-bottom:20px;padding-left:20px;padding-right:20px;padding-top:0px;'> " +
            "<table width='100%' align='center' cellpadding='0' cellspacing='0' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;margin:0 auto;max-width:530px;mso-table-lspace:0;mso-table-rspace:0;width:100%;'> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#4a4a4a;font-size:28px;font-weight:400;line-height:36px;mso-line-height-rule:exactly;padding-bottom:20px;padding-top:30px;text-align:center;letter-spacing:1px;'> <b>Welcome to Changemakers World!</b> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#4a4a4a;font-size:28px;font-weight:400;line-height:36px;mso-line-height-rule:exactly;padding-bottom:10px;text-align:center;letter-spacing:1px;'> <a href='http://portal.changemakersworld.com/'><img src='https://image.ibb.co/cjWCk8/cmw_activation_process.jpg' alt='cmw_activation_process' style='height:150px; border-radius:10px;'></a> <br /> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-font-smoothing:subpixel-antialiased;-webkit-text-size-adjust:100%;color:#2a2c39;font-size:18px;line-height:26px;mso-line-height-rule:exactly;text-align:left;padding-bottom:20px;'>" +
            " We've received your registration request. Please, click the following button to activate your account: </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-bottom:10px;text-align:center;'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:separate;max-width:220px;min-width:70px;mso-table-lspace:0;mso-table-rspace:0;width:100%;' align='center' width='100%' cellpadding='0' cellspacing='0'> <tbody> <tr> <td height='50' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#2fb8f1;border:1px solid #2fb8f1;border-radius:3px;height:auto;mso-line-height-rule:exactly;text-align:center;vertical-align:middle;'> <a href='" + ActivationUrl + "' target='_blank' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#2fb8f1;color:#fff !important;display:block;font-size:18px;mso-line-height-rule:exactly;padding:13px 0;text-align:center;text-decoration:none;white-space:normal;'> <span style='color:#fff;text-transform:uppercase;'>activate</span> </a> </td> </tr> <tr><td height='50' style='text-align:center;vertical-align:middle; color:red;padding-top:20px;'>Note that this link will be available for only 1 hour</td></tr><tr><td height='50' style='text-align:center;vertical-align:middle; color:black;padding-top:20px;'>If above activation link does not work please <a href='" + ActivationUrl + "'> click here</a> </td></tr></tbody> </table> </td> </tr>" +
            " </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#f7f7f7;mso-line-height-rule:exactly;table-layout:fixed;text-align:center;' align='center' valign='top'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:640px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-bottom:20px;padding-left:20px;padding-right:20px;padding-top:20px;vertical-align:top;'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;margin:0 auto;max-width:500px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr>" +
            " <td style='-ms-text-size-adjust:100%;-webkit-font-smoothing:subpixel-antialiased;-webkit-text-size-adjust:100%;color:#a8a9ad;font-size:14px;line-height:20px;mso-line-height-rule:exactly;text-align:center;vertical-align:top;'> You are receiving this email because you registered on <a href='http://portal.changemakersworld.com/' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#2fb8f1 !important;mso-line-height-rule:exactly;text-decoration:none;' target='_blank'><span style='color:#2fb8f1'>Changemaker world</span></a> and subscribed to receive email updates about Changemakers users and ideas. To manage your notifications go to <a href='http://portal.changemakersworld.com/' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#2fb8f1 !important;mso-line-height-rule:exactly;text-decoration:none;' target='_blank'><span style='color:#2fb8f1'>http://portal.changemakersworld.com/</span></a>. </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></td> </tr></tbody></table></body></html>";

            try
            {
                WebMail.Send(email,
                          "Changemakers World - " + "Activate Your CMW Account",
                          MessageBodyAtivate + "<br/><br/> If above link does't work <a href='" + ActivationUrl + "'> click here</a>",
                          null, null, null,
                          true,
                          null, null, null, null, null,
                          email
                          );
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public class IsNetworkAvail
        {
            [DllImport("wininet.dll")]
            private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);
            public bool IsConnected()
            {
                int Desc;
                InternetGetConnectedState(out Desc, 0).ToString();
                if (Desc == 16)
                    return false;
                else
                    return true;
            }

        }
        public ActionResult Verify()
        {
            string message = "";
            ViewBag.VerifyMessage = message;
            return View();
        }
        [HttpPost]
        public ActionResult Verify(VerifyViewModel verify)
        {
            if (ModelState.IsValid)
            {
                var UaerName = ChangeMakerEntities.RUserName;
                string message = "Invalid Verification Code";
                if (OTP == verify.EnterVerificationCode)
                {
                    CME.ActivateUser(UaerName);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.VerifyMessage = message;
                }
            }
            return View();
        }
        public JsonResult VerifyEmail(string email, bool IsVerification)
        {
            bool Result = false;
            if (Result = CME.VerifyEmail(email))
            {
                if (IsVerification)
                {
                    UserController.IsVerification = true;
                    if (Result = SendVerificationEmail(email))
                    {
                        return Json(Result, JsonRequestBehavior.AllowGet);
                    }
                    return Json(false);
                }
                else
                {
                    UserController.IsVerification = false;
                    if (Result = SendVerificationMail(email))
                    {
                        return Json(Result, JsonRequestBehavior.AllowGet);
                    }
                    return Json(false);
                }
                
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        
        [MethodImpl(MethodImplOptions.Synchronized)]
        private bool SendVerificationMail(string email)
        {
            string ActivationUrl = "";
            bool Astatus = false;
          //  ActivationUrl = Server.HtmlEncode("http://localhost:54165/Home/Verification?email=" + email + "&key=" + GetKey(email) + "&Astatus=" + Astatus);
             ActivationUrl = Server.HtmlEncode("https://portal.changemakersworld.com/Home/Verification?email=" + email + "&key=" + GetKey(email)+ "&Astatus="+Astatus);

            string MessageBodyReset = "<html><body><table width='100%' align='center' cellpadding='0' cellspacing='0' border='0' style='height:100%;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;padding:0;width:100%;'> <tbody> <tr> <td align='center' valign='top' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;margin:0;background-color:#f7f7f7;height:100%;mso-line-height-rule:exactly;padding:0;width:100%;'> <table width='100%' cellpadding='0' cellspacing='0' border='0' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;'> <tbody> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#f7f7f7;mso-line-height-rule:exactly;' align='center' valign='top'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:640px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr>" +
            " <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-bottom:17px;padding-left:20px;padding-right:20px;padding-top:20px;text-align:left;'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:530px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr> <td width='50%' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-right:20px;'> <img src='https://image.ibb.co/nsFYA8/favicon_32x32.png' style=' margin-bottom:-10px;' /><label style='margin-left:5px;margine-top=-10px;'><b>Changemakers World</b></label> </td> <td width='50%' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;'> <table align='right' cellpadding='0' cellspacing='0' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;'> <tbody> <tr> <td width='20' " + " style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-right:10px;'> <img src='https://gallery.mailchimp.com/4af5a9e2e2d51c22c87e8d44a/images/7d634ffd-2892-4e6d-9ba4-3ee5e8813155.png' " +
            " width='21' height='19' alt='' style='-ms-interpolation-mode:bicubic'> </td> <td style='-ms-text-size-adjust:100%;-webkit-font-smoothing:subpixel-antialiased;-webkit-text-size-adjust:100%;color:#2a2c39;font-family:' Trebuchet MS, Lucida Grande, Lucida Sans Unicode, Lucida Sans, Tahoma, sans-serif';font-size:12px;line-height:18px;mso-line-height-rule:exactly;text-align:left;padding-bottom:3px;font-weight:700;white-space:nowrap;'>" + EFName + "</td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#f7f7f7;mso-line-height-rule:exactly;' align='center' valign='top'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:600px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#fff;mso-line-height-rule:exactly;padding-bottom:20px;padding-left:20px;padding-right:20px;padding-top:0px;'> " +
            "<table width='100%' align='center' cellpadding='0' cellspacing='0' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;margin:0 auto;max-width:530px;mso-table-lspace:0;mso-table-rspace:0;width:100%;'> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#4a4a4a;font-size:28px;font-weight:400;line-height:36px;mso-line-height-rule:exactly;padding-bottom:20px;padding-top:30px;text-align:center;letter-spacing:1px;'> <b>Welcome to Changemakers World!</b> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#4a4a4a;font-size:28px;font-weight:400;line-height:36px;mso-line-height-rule:exactly;padding-bottom:10px;text-align:center;letter-spacing:1px;'> <a href='http://portal.changemakersworld.com/'><img src='https://image.ibb.co/cjWCk8/cmw_activation_process.jpg' alt='cmw_activation_process' style='height:150px; border-radius:10px;'></a> <br /> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-font-smoothing:subpixel-antialiased;-webkit-text-size-adjust:100%;color:#2a2c39;font-size:18px;line-height:26px;mso-line-height-rule:exactly;text-align:left;padding-bottom:20px;'>" +
            " We've received your Reset Password request. Please, click the following button to Reset Password: </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-bottom:10px;text-align:center;'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:separate;max-width:220px;min-width:70px;mso-table-lspace:0;mso-table-rspace:0;width:100%;' align='center' width='100%' cellpadding='0' cellspacing='0'> <tbody> <tr> <td height='50' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#2fb8f1;border:1px solid #2fb8f1;border-radius:3px;height:auto;mso-line-height-rule:exactly;text-align:center;vertical-align:middle;'> <a href='" + ActivationUrl + "' target='_blank' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#2fb8f1;color:#fff !important;display:block;font-size:18px;mso-line-height-rule:exactly;padding:13px 0;text-align:center;text-decoration:none;white-space:normal;'> <span style='color:#fff;text-transform:uppercase;'>Reset Password</span> </a> </td> </tr> <tr><td height='50' style='text-align:center;vertical-align:middle; color:red;padding-top:20px;'>Note that this link will be available for only 1 hour</td></tr><tr><td height='50' style='text-align:center;vertical-align:middle; color:black;padding-top:20px;'>If above activation link does not work please <a href='" + ActivationUrl + "'> click here</a> </td></tr> </tbody> </table> </td> </tr>" +
            " </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;background-color:#f7f7f7;mso-line-height-rule:exactly;table-layout:fixed;text-align:center;' align='center' valign='top'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;max-width:640px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr> <td style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;mso-line-height-rule:exactly;padding-bottom:20px;padding-left:20px;padding-right:20px;padding-top:20px;vertical-align:top;'> <table style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;border-collapse:collapse;margin:0 auto;max-width:500px;mso-table-lspace:0;mso-table-rspace:0;' width='100%' align='center' cellpadding='0' cellspacing='0'> <tbody> <tr>" +
            " <td style='-ms-text-size-adjust:100%;-webkit-font-smoothing:subpixel-antialiased;-webkit-text-size-adjust:100%;color:#a8a9ad;font-size:14px;line-height:20px;mso-line-height-rule:exactly;text-align:center;vertical-align:top;'> You are receiving this email because you registered on <a href='http://portal.changemakersworld.com/' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#2fb8f1 !important;mso-line-height-rule:exactly;text-decoration:none;' target='_blank'><span style='color:#2fb8f1'>Changemaker world</span></a> and subscribed to receive email updates about Changemakers users and ideas. To manage your notifications go to <a href='http://portal.changemakersworld.com/' style='-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#2fb8f1 !important;mso-line-height-rule:exactly;text-decoration:none;' target='_blank'><span style='color:#2fb8f1'>http://portal.changemakersworld.com/</span></a>. </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table></td> </tr></tbody></table></body></html>";

            string SubjectforReset = "Reset Password of Your CMW Account";
          
            try
            {
                WebMail.Send(email,
                          "Changemakers World - "+ SubjectforReset,
                          MessageBodyReset+ "<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If Above Link Does't Work Please <a href='" + ActivationUrl+"'> Click Here</a>",
                          null, null, null,
                          true,
                          null, null, null, null, null,
                          email
                          );
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
     //   public static System.Timers.Timer tm = new System.Timers.Timer();
        public static string EmailForResetPass = "";
        public ActionResult Verification(string email, string key, bool Astatus)
        {
            var user = CMDB.tblMembers.FirstOrDefault(x => x.Email == email);
            if (user != null)
            {
                var user1 = CMDB.tblKits.FirstOrDefault(x => x.Key == key);
                if (user1 != null)
                {
                    if (Astatus)
                    {
                        CME.ActivateUser(user.UserName);
                        return Redirect("~/HtmlPages/Activation.html");
                    }
                    else
                    {
                        EmailForResetPass = email;
                        return RedirectToAction("ResetPassword", "User");
                    }
                }
                return Redirect("~/Error/Error.html");
            }
            else
            {
                return Redirect("~/Error/Error.html");
            }
        }
      //[HttpPost]
      //  [ValidateAntiForgeryToken]
        public ActionResult Login12(LoginViewModel lvm)
        {
            if (Session["MemberId"] != null && Session["SessionId"] != null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    bool IsValid = CME.LoginInfo(lvm);
                    if (IsValid)
                    {
                        int memberid = Convert.ToInt32(Session["MemberId"]);
                        if (CME.IsActive(memberid))
                        {
                            return RedirectToAction("Index", "Home"); //1 Shows Successfull login
                        }
                        else
                        {
                            Session.RemoveAll();
                            return Json(true); //2 InActive User
                        }
                    }
                    else
                        return Json(false); //3 shows invalid details.
                }
                return Json(false); //3  Invalid login data
            }
        }
    }
}
