﻿using ChangeMakers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChangeMakers.Controllers
{
    public class AdminController : Controller
    {
        ChangeMakerDBEntities CMDB = new ChangeMakerDBEntities();
        ChangeMakerEntities CME = new ChangeMakerEntities();
        // GET: Admin
        public ActionResult AdminPannel()
        {
            List<tblLevel> lvl = CMDB.tblLevels.ToList();
            ViewBag.ListOfLevelId = new SelectList(lvl, "LevelId", "LevelNo");
            List<tblReferralCode> rc = CMDB.tblReferralCodes.ToList();
            var ReferralId = new SelectList(rc, "ReferralId", "ReferralId");
            ViewBag.ListOfReferralId = ReferralId;
            ViewBag.ListOfReferralName = new SelectList(rc, "ReferralId", "ReferralId");
            List<tblMember> user = CMDB.tblMembers.ToList();
            ViewBag.RefferedBy = new SelectList(user, "MemberId", "FirstName");
            return View();
        }
        public JsonResult GetUserList()
        {
            List<MemberViewModel> UserDetails = CME.GetAllMemberList();
            return Json(UserDetails, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUserById(int UserId)
        {
            string value = string.Empty;
            value = CME.GetAllUserInfo(UserId);
            return Json(value, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveUserDetails(MemberViewModel mvm)
        {
            var errors = ModelState.Where(x => x.Value.Errors.Any())
                .Select(x => new { x.Key, x.Value.Errors });
            if (ModelState.IsValid)
            {
                bool IsValid = false;
                if (IsValid = CME.IsValidUser(mvm.UserName))
                {
                    if (IsValid = CME.IsValidRID(mvm.ReferralCode))
                    {
                        if(mvm.MemberId>0)
                        {
                            IsValid = CME.UpdateUsersByAdmin(mvm);
                            return Json(IsValid);
                        }
                        else
                        {
                            IsValid = CME.InsertUserByAdmin(mvm);
                            return Json(true);
                        }
                    }
                    else
                        return Json(false);
                }
                else
                    return Json(false);
            }
            return Json(false);
        }
        public JsonResult DeleteMemeber(int DUserid)
        {
            bool result = false;
            result = CME.DeleteMemeber(DUserid);
            return Json(result);
        }
        public ActionResult Login()
        {
            return View();
        }
    }
}