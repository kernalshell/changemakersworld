﻿using ChangeMakers.Models;
using ChangeMakers.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChangeMakers.AccessClasses;
using System.IO;
using System.Web.Helpers;

namespace ChangeMakers.Controllers
{
    public class UserController : Controller
    {
        ChangeMakerDBEntities CMDB = new ChangeMakerDBEntities();
        ChangeMakerEntities CME = new ChangeMakerEntities();
        ReqResp RR = new ReqResp();
        
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Registration()
        {
            return View();
        }
        public ActionResult Login()
        {
            return RedirectToAction("Login", "Home");
           // return View();
        }
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult MemberSignUp(SignUpViewModel svm)
        {
            if (ModelState.IsValid)
            {
                bool IsValid = false;
                if (IsValid = CME.IsValidUser(svm.UserName))
                {
                    if (IsValid = CME.IsValidRID(svm.ReferralCode))
                    {
                        if (IsValid = RR.SendVerificationMail(svm.Email))
                        {
                            // IsValid = CME.InsertInMember(svm);
                            // return RedirectToAction("Login", "Home");
                            return RedirectToAction("Verify", "Home");
                        }

                    }
                    else
                    {
                        ViewBag.Message = "Invalid Referance Id";
                        return View();
                    }
                }
                else
                {
                    ViewBag.Message = "User Name Already Exist";
                    return View();
                }
            }
            ViewBag.Message = "";
            return View();
        }
        public ActionResult UserProfile()
        {
            if (Session["MemberId"] == null || Session["SessionId"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            else
            {
                int mid = Convert.ToInt32(Session["MemberId"]);
                ViewBag.MemberId = mid;
                
                ViewBag.MyRefCode = ChangeMakerEntities.MyRefCode;
                ViewBag.TotakKits = ChangeMakerEntities.TotalKit;
                UserDetails();
                return View();
            }
        }
        public int CountActiveMember(int id)
        {
            int Count = CMDB.tblReferralCodes.Count(x => x.MemberId == id);
            return Count;
        }
        public int TotalTokens(int Lvlid)
        {
            int totaltokens = 0;
            if (Lvlid == 1)
            {
                totaltokens = 9 * 1500;
            }
            else if (Lvlid == 2)
            {
                totaltokens = 6 * 1500;
            }
            else if (Lvlid == 3)
            {
                totaltokens = 3 * 1500;
            }
            else
            {
                totaltokens = 1500;
            }
            return totaltokens;
        }
       
        public ActionResult TreeView()
        {
            if (Session["MemberId"] != null && Session["SessionId"] != null)
            {
                int mid =Convert.ToInt32(Session["MemberId"]);
                if(!CME.IsAcceptedRP(mid))
                {
                    return RedirectToAction("Index", "Home");
                }
                var MyMember = CME.GetMyMember(mid);
                ViewBag.MyMember = MyMember;
                UserDetails();
                tblMember user=  CMDB.tblMembers.FirstOrDefault(x => x.MemberId == ChangeMakerEntities.AuthID);
                if(user!=null)
                {
                    return View(user);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }

        private void UserDetails()
        {
            if (Session["memberid"] != null && Session["sessionid"] != null)
            {
                int mid = Convert.ToInt32(Session["memberid"]);
                ViewBag.MyRefCode = CME.MyReferCode(mid);
                ViewBag.UserProfilePic = CME.GetProfilePic(mid);
                var newuser = CMDB.tblMembers.FirstOrDefault(x => x.MemberId == mid);
                if (newuser != null)
                {
                    ViewBag.Collection = newuser;
                    if (newuser.ReferralId != null)
                    {
                        ViewBag.AuthRefCode = CME.code((int)newuser.ReferralId);
                        ViewBag.AuthRefName = ChangeMakerEntities.AuthRefName;
                    }
                }
            }
        }

        public static bool IsVerification = false;
        public ActionResult Verification()
        {
            IsVerification = true;
            ViewBag.Message = "";
            return View();
        }
        public JsonResult UpdateUserDetails(int MemberId,string FirstName,string LastName,string MobileNo)
        {
            bool Result = false;
            long mobileNo = 0;
            if (MobileNo == "") { mobileNo = 0; } else { mobileNo = Convert.ToInt64(MobileNo); }
            Result = CME.UpdateUserByUser(MemberId, FirstName, LastName, mobileNo);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ResetPass( string email,string pass)
        {
            JsonResult result;
            if (email == "") { return Json(false); }
            try
            {
                var user = CMDB.tblMembers.FirstOrDefault(m => m.Email == email);
                if(user!=null)
                {
                    result = UpdateUserPass(user.MemberId, pass);
                    return result;
                }
                else
                {
                    return Json(false);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JsonResult UpdateUserPass(int MemberId, string Password)
        {
            bool Result = false;
            Result = CME.UpdatePassByUser(MemberId, Password);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ForgatePassword()
        {
            IsVerification = false;
            ViewBag.Message = "";
            return View();
        }
        public ActionResult ResetPassword()
        {
            ViewBag.Message = HomeController.EmailForResetPass;
            return View();
        }
        public ActionResult KYC()
        {
            if (Session["MemberId"] != null && Session["SessionId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public ActionResult Referral()
        {
            if (Session["memberid"] != null && Session["sessionid"] != null)
            {
                int mid = Convert.ToInt32(Session["memberid"]);
                if (CME.IsAcceptedRP(mid))
                    return RedirectToAction("ReferralProgram", "User");
               
                UserDetails();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public ActionResult ReferralProgram()
        {
            if (Session["memberid"] != null && Session["sessionid"] != null)
            {
                int mid = Convert.ToInt32(Session["memberid"]);
                UserDetails();
                var MyMember = CME.GetMyMember_Inc(mid);
                ViewBag.MyMember = MyMember;
                ViewBag.TotalRefInc = ChangeMakerEntities.TotalRefInc;
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
            //return View();
            //return RedirectToAction("Index", "Home");
        }
        public ActionResult Accepted(bool Yes)
        {
            if (Session["MemberId"] != null && Session["SessionId"] != null)
            {
                int mid = Convert.ToInt32(Session["MemberId"]);
                bool Result = false;
                Result = CME.AcceptedRP(mid);
                return Json(Result);
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }

           // return RedirectToAction("Index", "Home");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public JsonResult UploadFile(KYCViewModel KYCData)
        {
            bool Result = false;
            int mid = 0;
            if (Session["MemberId"] != null && Session["SessionId"] != null)
            {
                mid = Convert.ToInt32(Session["MemberId"]);
                if(CME.IsKYCOK(mid))
                {
                    return Json("KYC is Ok", JsonRequestBehavior.AllowGet);
                }
            }
            else
                return Json(Result, JsonRequestBehavior.AllowGet);
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                string user = CME.GetUserNameById(mid);
                if(user=="")
                {
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                HttpPostedFileBase Pancardfile = new HttpPostedFileWrapper(System.Web.HttpContext.Current.Request.Files["Pancardfile"]);
                HttpPostedFileBase Frontsidefile =new HttpPostedFileWrapper(System.Web.HttpContext.Current.Request.Files["Frontsidefile"]);
                HttpPostedFileBase Backsidefile =new HttpPostedFileWrapper(System.Web.HttpContext.Current.Request.Files["Backsidefile"]);
                List<HttpPostedFileBase> ImageList = new List<HttpPostedFileBase>();
                ImageList.Add(Pancardfile); ImageList.Add(Frontsidefile); ImageList.Add(Backsidefile);
                if (Result = RR.UploadImage(ImageList,user))
                {
                    //string PancardfileEXT = Path.GetExtension(Pancardfile.FileName);
                    //string FrontsidefileEXT = Path.GetExtension(Frontsidefile.FileName);
                    //string BacksidefileEXT = Path.GetExtension(Backsidefile.FileName);
                    string PanImageUrl = "https://s3.us-east-2.amazonaws.com/cmwkyc/kycpic/" + user + "PANImage";
                    string ProofFrontUrl = "https://s3.us-east-2.amazonaws.com/cmwkyc/kycpic/" + user + "proofFrontSide";
                    string ProofBackUrl = "https://s3.us-east-2.amazonaws.com/cmwkyc/kycpic/" + user + "ProofBackSide";
                    KYCData.PanImage = PanImageUrl;  KYCData.ProofFront = ProofFrontUrl;
                    KYCData.ProofBack = ProofBackUrl; KYCData.MemberId = mid;
                    var errors = ModelState.Where(x => x.Value.Errors.Any())
               .Select(x => new { x.Key, x.Value.Errors });
                    if (Result = CME.InsertintoKYC(KYCData,mid))
                    { 
                        return Json(Result, JsonRequestBehavior.AllowGet);
                    }
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Encrypt(ReferralVeiwModel RVM)
        {
            string Encryptedrc = string.Empty;
            string Encrypteddt = string.Empty;
            Encryptedrc = RR.Encrypt(RVM.Rcodestr);
            Encrypteddt = RR.Encrypt(RVM.Datestr);
            if (Encryptedrc != "" && Encrypteddt != "")
            {
                RVM.Datestr = Encrypteddt;
                RVM.Rcodestr = Encryptedrc;
                return Json(RVM, JsonRequestBehavior.AllowGet);
            }
            return Json(false);
        }
        public JsonResult Decrypt(ReferralVeiwModel RVM)
        {
            string Decryptedrc = string.Empty;
            string Decrypteddt = string.Empty;
            Decryptedrc = RR.Decrypt(RVM.Rcodestr);
            Decrypteddt = RR.Decrypt(RVM.Datestr);
            if (Decryptedrc != "" && Decrypteddt != "")
            {
                RVM.Datestr = Decrypteddt;
                RVM.Rcodestr = Decryptedrc;
                int RefdMid = CME.RefCodeToMId(Decryptedrc);
                //int mid = CME.GetMIdByRId(RefdMid);
                string userinfo = CME.GetUserFullNameById(RefdMid);
                RVM.RefdName = userinfo;
                return Json(RVM, JsonRequestBehavior.AllowGet);
            }
            return Json(false);
        }
        public ActionResult Cources()
        {
            if (Session["memberid"] != null && Session["sessionid"] != null)
            {
                int mid = Convert.ToInt32(Session["memberid"]);
                UserDetails();
                var MyMember = CME.GetMyMember_Inc(mid);
                ViewBag.MyMember = MyMember;
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
        }
        public JsonResult UploadProfilePic()
        {
            bool Result = false;
            int mid = 0;
            if (Session["MemberId"] != null && Session["SessionId"] != null)
            {
                mid = Convert.ToInt32(Session["MemberId"]);
            }
            else
                return Json(Result, JsonRequestBehavior.AllowGet);
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                string user = CME.GetUserNameById(mid);
                if (user == "")
                {
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                HttpPostedFileBase Userprofilepic = new HttpPostedFileWrapper(System.Web.HttpContext.Current.Request.Files["Userprofilepic"]);
                if (Result = RR.UploadProfilePic(Userprofilepic, user))
                {
                    string UserProfileurl = "https://s3.us-east-2.amazonaws.com/cmwkyc/profilepic/" + user + "ProfilePic";
                    var errors = ModelState.Where(x => x.Value.Errors.Any())
                    .Select(x => new { x.Key, x.Value.Errors });
                    if (Result = CME.InsertProfilePic(UserProfileurl, mid))
                    {
                        return Json(UserProfileurl, JsonRequestBehavior.AllowGet);
                    }
                    return Json(Result, JsonRequestBehavior.AllowGet);
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        
    }   
}