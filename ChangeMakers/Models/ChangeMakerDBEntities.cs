﻿using ChangeMakers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.Web.Mvc;
using ChangeMakers.Controllers;

namespace ChangeMakers.Models
{
    public class ChangeMakerEntities
    {
        public HttpSessionStateBase Session { get; }
        ChangeMakerDBEntities CMDBE = new ChangeMakerDBEntities();
        public static string UsrFName = string.Empty;
        public static string UsrLName = string.Empty;
        public static int AuthID = 0;
        public static int LevelID = 0;
        public static string AuthRefName = string.Empty;
        public static string AuthRefByCode = string.Empty;
        public static string MyRefCode = string.Empty;
        public static int TotalKit = 0;
        public static string RUserName = string.Empty;
        public bool IsActive(int mid)
        {
            bool result = false;
            var user = CMDBE.tblMembers.FirstOrDefault(u => u.MemberId == mid && u.IsActivated == true);
             if(user!=null)
            {
                result = true;
            }
            return result;
        }

        public bool LoginInfo(LoginViewModel logininfo)
        {
           
            bool IsValid = false;
            try {
                var user = CMDBE.tblMembers.FirstOrDefault(u => u.UserName == logininfo.UserName);
                if (user != null)
                {
                    if (user.Password == logininfo.Password)
                    {
                        SessionIDManager manager = new SessionIDManager();
                        string newSessionId = manager.CreateSessionID(HttpContext.Current);
                        HttpContext.Current.Session["MemberId"] = user.MemberId.ToString();
                        HttpContext.Current.Session["SessionId"] = newSessionId;
                        AuthID = user.MemberId;
                        UsrFName = user.FirstName;
                        UsrLName = user.LastName;
                        LevelID = user.LevelId;
                        if (user.ReferralId != null)
                            AuthRefByCode = code((int)user.ReferralId);
                        MyRefCode = MyReferCode(user.MemberId);

                        var newdata = CMDBE.tblKits.FirstOrDefault(x => x.MemberId == AuthID);
                        if (newdata != null) { TotalKit = Convert.ToInt32(newdata.NoOfKits); }
                        IsValid = true;
                    }
                }
            }
            catch(Exception ex)
            {
               // ModelState.AddModelError("", ErrorCodeToString(ex.StatusCode));
            }
            return IsValid;
        }

        public string MyReferCode(int memberId)
        {
            try
            {
                var user1 = CMDBE.tblReferralCodes.FirstOrDefault(x => x.MemberId == memberId);
                if (user1 != null)
                {
                    return user1.ReferralCode;
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string code(int rid)
        {
            var users = CMDBE.tblReferralCodes.FirstOrDefault(x => x.ReferralId == rid);
            if(users!=null)
            {
                var newuser = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == users.MemberId);
                if(newuser!=null)
                {
                    AuthRefName = newuser.FirstName +" "+ newuser.LastName;
                }
                return users.ReferralCode;

            }
            return "";
        }





        /////////////////////////////////////////////// Admin Code  ////////////////////////////////////////////////

        public List<MemberViewModel> GetAllMemberList()
        {
            List<MemberViewModel> MachList = CMDBE.tblMembers.Select(x => new MemberViewModel
            {
                MemberId = x.MemberId,
                UserName = x.UserName,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                MobileNo = (long)x.MobileNo,
                Password = x.Password,
                LevelId = x.LevelId,
                ReferralId = (int)(x.ReferralId),
                CreatedAt = x.CreatedAt,
                ModifiedAt = x.ModifiedAt,
                IsActivated = x.IsActivated,
                AdvanceBalance = (long)x.AdvanceBalance,
                DueBalance = (long)x.DueBalance,
                CryptoAmt = (double)(x.CryptoAmt),
                CryptoType = x.CryptoType
            }).ToList();
            
            //            var data = CMDBE.tblMembers.ToList();
            return MachList;
        }

        public bool UpdatePassByUser(int memberId, string password)
        {
            tblMember tblm = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == memberId);
            if (tblm != null)
            {
                tblm.Password = password;
                CMDBE.SaveChanges();
                return true;
            }
            return false;
        }

        public bool UpdateUserByUser(int memberId, string firstName, string lastName, long mobileNo)
        {
            tblMember tblm = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == memberId);
            if(tblm!=null)
            {
                if (firstName != null) {tblm.FirstName = firstName; }
                if (lastName != null) { tblm.LastName = lastName;}
                if (mobileNo != 0) { tblm.MobileNo = mobileNo;}
                CMDBE.SaveChanges();
                return true;
            }
            return false;
        }
        public bool UpdateUsersByAdmin(MemberViewModel mvm)
        {
            tblMember tblm = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == mvm.MemberId);
            tblm.UserName = mvm.UserName;
            tblm.FirstName = mvm.FirstName;
            tblm.LastName = mvm.LastName;
            tblm.Email = mvm.Email;
            tblm.MobileNo = mvm.MobileNo;
            tblm.Password = mvm.Password;
            tblm.LevelId = lvlid(mvm.LevelId);
            tblm.CreatedAt = mvm.CreatedAt;
            tblm.ModifiedAt = mvm.ModifiedAt;
            tblm.AdvanceBalance = mvm.AdvanceBalance;
            tblm.DueBalance = mvm.DueBalance;
            tblm.CryptoAmt = mvm.CryptoAmt;
            tblm.CryptoType = mvm.CryptoType;
            CMDBE.SaveChanges();
            return true;
        }
        public bool InsertUserByAdmin(MemberViewModel mvm)
        {
            tblMember tblm = new tblMember();
            tblm.UserName = mvm.UserName;
            tblm.FirstName = mvm.FirstName;
            tblm.LastName = mvm.LastName;
            tblm.Email = mvm.Email;
            tblm.MobileNo = mvm.MobileNo;
            tblm.Password = mvm.Password;
            tblm.LevelId = lvlid(mvm.LevelId);
            tblm.CreatedAt = mvm.CreatedAt;
            tblm.ModifiedAt = DateTime.Now.Date;
            tblm.AdvanceBalance = mvm.AdvanceBalance;
            tblm.DueBalance = mvm.DueBalance;
            tblm.CryptoAmt = mvm.CryptoAmt;
            tblm.CryptoType = mvm.CryptoType;
            CMDBE.tblMembers.Add(tblm);
            CMDBE.SaveChanges();
            bool IsOK = InsertIntblRefCode(mvm.UserName);
            return IsOK;
        }

        public bool IsEmailExist(string email)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(x => x.Email == email);
                if (user != null)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool DeleteMemeber(int id)
        {
            bool result = false;
            tblReferralCode ref1 = CMDBE.tblReferralCodes.SingleOrDefault(x => x.MemberId == id);
            if(ref1!=null)
            {
                CMDBE.tblReferralCodes.Remove(ref1);
                CMDBE.SaveChanges();
                tblMember tblm = CMDBE.tblMembers.SingleOrDefault(x => x.MemberId == id);
                if(tblm!=null)
                {
                    CMDBE.tblMembers.Remove(tblm);
                    CMDBE.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        internal bool IsKYCOK(int mid)
        {
            try
            {
                var user = CMDBE.tbkKYCs.FirstOrDefault(x => x.MemberId == mid);
                if(user!=null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object GetUserInfoById(int mid)
        {
            try
            {
                var User = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == mid);
                if (User != null)
                {
                    return User;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public object GetMyMember(int mid)
        {
            int rid = GetMyRidByMid(mid);
            if (rid!=0)
            {
                var memberlist = GetMemberListByRid(rid);
                if(memberlist!=null)
                {
                    return memberlist;
                }
                return null;
            }
            return null;
        }
        public static int TotalRefInc = 0;
        public List<MyMemebersViewModel> GetMyMember_Inc(int mid)
        {
            TotalRefInc = 0;
            List<MyMemebersViewModel> MyMemList = new List<MyMemebersViewModel>();
            try
            {
                int myRid = GetMyRidByMid(mid);
                var user = CMDBE.tblMembers.FirstOrDefault(m => m.MemberId == mid);
                List<tblMember> tblm = CMDBE.tblMembers.Where(r => r.ReferralId == myRid).ToList();
                foreach (var item in tblm)
                {
                    MyMemebersViewModel MyMem = new MyMemebersViewModel();
                    var tblkit = CMDBE.tblKits.FirstOrDefault(k => k.MemberId == item.MemberId);
                    int thisKits = Convert.ToInt32(tblkit.NoOfKits);
                    int MyLevel = user.LevelId;
                    int MyLvlPer = GetLvlPerByLvlId(MyLevel);
                    int thisIncforme = thisKits * 1500 * MyLvlPer / 100;
                    MyMem.FirstName = item.FirstName;
                    MyMem.LastName = item.LastName;
                    MyMem.Level = item.LevelId;
                    MyMem.Incentive = thisIncforme;
                    MyMemList.Add(MyMem);
                    TotalRefInc = TotalRefInc + thisIncforme;
                }
                if(MyMemList!=null)
                {
                    return MyMemList;
                }
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private int GetMyRidByMid(int mid)
        {
            try
            {
                var user = CMDBE.tblReferralCodes.FirstOrDefault(m => m.MemberId == mid);
                if(user!=null)
                {
                    return user.ReferralId;
                }
                return 0;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private object GetMemberListByRid(int rid)
        {
            try
            {
                var mlist = CMDBE.tblMembers.Where(x => x.ReferralId == rid).Select(x => x.FirstName).ToList();
                if(mlist!=null)
                {
                    return mlist;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }


        /////////////////////////////////////////   General   //////////////////////////////////////////////////

        public int lvlid(int levelId)
        {
            int lvlid = 0;
            switch (levelId)
            {
                case 1:
                    lvlid = 6;
                    break;
                case 2:
                    lvlid = 6;
                    break;
                case 3:
                    lvlid = 6;
                    break;
                case 4:
                    lvlid = 5;
                    break;
                case 7:
                    lvlid = 4;
                    break;
                case 10:
                    lvlid = 3;
                    break;
                case 16:
                    lvlid = 2;
                    break;
                case 25:
                    lvlid = 1;
                    break;
            }
            return lvlid;
        }

        public bool IsValidUser(string userName)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(x => x.UserName == userName);
                if (user != null)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public bool IsValidRID(string referralCode)
        {
            if(referralCode!=null)
            {
                var user = CMDBE.tblReferralCodes.FirstOrDefault(x => x.ReferralCode == referralCode);
                if (user != null)
                    return true;
                return false;
            }
            return true;
        }

        public bool InsertInMember(MemberViewModel mvm)
        {
            bool Result = false;
            try
            {
                tblMember tblm = new tblMember();
                tblm.UserName = mvm.UserName;
                tblm.FirstName = mvm.FirstName;
                tblm.LastName = mvm.LastName;
                tblm.Email = mvm.Email;
                tblm.MobileNo = mvm.MobileNo;
                tblm.Password = mvm.Password;
                tblm.LevelId = lvlid(mvm.LevelId);
                tblm.ReferralId = RefCodeToRefId(mvm.ReferralCode);
                tblm.CreatedAt = DateTime.Now.Date;
                tblm.ModifiedAt = DateTime.Now.Date;
                tblm.AdvanceBalance = mvm.AdvanceBalance;
                tblm.DueBalance = mvm.DueBalance;
                tblm.CryptoAmt = mvm.CryptoAmt;
                tblm.CryptoType = mvm.CryptoType;
                CMDBE.tblMembers.Add(tblm);
                CMDBE.SaveChanges();

                Result = InsertIntblRefCode(mvm.UserName);
                Result = InsertInKits(mvm.UserName, mvm.LevelId);
                RUserName = mvm.UserName;
                UpdateIncentive_Promotion(mvm.ReferralCode, mvm.LevelId);
                return Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
                          
        }

        internal string GetUserFullNameById(int mid)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(m => m.MemberId == mid);
                if(user!=null)
                {
                    string fullname = user.FirstName + " " + user.LastName;
                    return fullname;
                }
                return "";
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool InsertInKits(string userName,int levelId)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(x => x.UserName == userName);
                if (user != null)
                {
                    tblKit tblK = new tblKit();
                    tblK.MemberId = user.MemberId;
                    tblK.NoOfKits = levelId;
                    tblK.Key = HomeController.RandomString(15);
                    CMDBE.tblKits.Add(tblK);
                    CMDBE.SaveChanges();
                    return true;
                }
                
            }
            catch (Exception)
            {
                throw;
            }
            return false;
        }

        public bool InsertProfilePic(string userProfileurl, int mid)
        {
            try
            {
                tblMember user = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == mid);
                if (user != null)
                {
                    user.ProfilePic = userProfileurl;
                    CMDBE.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetProfilePic(int mid)
        {
            string imgurl = "";
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == mid);
                if(user!=null)
                {
                    if(user.ProfilePic!=null) { imgurl = user.ProfilePic; }
                    else { imgurl = "https://s3.us-east-2.amazonaws.com/cmwkyc/profilepic/defaultprofilepic"; }
                    return imgurl;
                }
                return "";
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private int RefCodeToRefId(string referralCode)
        {
            try
            {
                var user = CMDBE.tblReferralCodes.FirstOrDefault(x => x.ReferralCode == referralCode);
                if (user != null)
                {
                    return user.ReferralId;
                }
                return 0;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool VerifyEmail(string email)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(x => x.Email == email);
                if (user != null)
                    return true;
            }
            catch (Exception)
            {
                throw;
            }
            return false;
        }

        private bool InsertIntblRefCode(string userName)
        {
            var user = CMDBE.tblMembers.FirstOrDefault(x => x.UserName == userName);
            if (user != null)
            {
                tblReferralCode tblR = new tblReferralCode();
                tblR.LevelNo = user.LevelId;
                tblR.MemberId = user.MemberId;
                tblR.ReferralCode = RandomString(6);
                tblR.CreatedAt = user.CreatedAt;
                tblR.ModifiedAt = user.ModifiedAt;
                CMDBE.tblReferralCodes.Add(tblR);
                CMDBE.SaveChanges();
                return true;
            }
            return false;
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "AB7CD0EF8GH1I9JKL2MNO3PQR4STU5VWX6YZ";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string GetAllUserInfo(int userId)
        {
            string value = string.Empty;
            tblMember model = CMDBE.tblMembers.Where(x => x.MemberId == userId).SingleOrDefault();
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return value;
        }

        public void ActivateUser(string username)
        {
            tblMember tblm = CMDBE.tblMembers.FirstOrDefault(x => x.UserName == username);
            if(tblm!=null)
            {
                tblm.IsActivated = true;
                CMDBE.SaveChanges();
            }
        }

        public bool IsAcceptedRP(int mid)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(v => v.MemberId==mid && v.IsAcceptedRP==1);
                if (user != null)
                    return true;
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool AcceptedRP(int mid)
        {
            try
            {
                tblMember tblm = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == mid);
                if (tblm != null)
                {
                    tblm.IsAcceptedRP = 1;
                    CMDBE.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool InsertintoKYC(KYCViewModel kYCData,int mid)
        {
            try
            {
                tbkKYC tblkyc = new tbkKYC();
                tblkyc.AddressProof = kYCData.AddressProof;
                tblkyc.MemberId = mid;
                tblkyc.PanNo = kYCData.PanNo;
                tblkyc.PanImage = kYCData.PanImage;
                tblkyc.PINCode = kYCData.PINCode;
                tblkyc.ProofNo = kYCData.ProofNo;
                tblkyc.State = kYCData.State;
                tblkyc.ProofFront = kYCData.ProofFront;
                tblkyc.ProofBack = kYCData.ProofBack;
                CMDBE.tbkKYCs.Add(tblkyc);
                CMDBE.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        public string GetUserNameById(int mid)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(m => m.MemberId == mid);
                if(user!=null)
                {
                    return user.UserName.ToString();
                }
                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateIncentive_Promotion(/*string NewUserName,*/ string RefCode, int NoOfKits)
        {
            int RefdMId = 0,ParentMId = 0,ParentRId = 0,Level1MId = 0;
           // int NewMid = GetMidByUserName(NewUserName);
            RefdMId = RefCodeToMId(RefCode);
            ParentMId = RefdMId;
           // int index = 0;   // i is index of memeber in chain
           // int[] ArrayOfTopMemebers = new int[50];
            while (true)
            {
                int TempRid = 0;
              //  ArrayOfTopMemebers[index] = ParentMId;
                if (IsLevel1Person(ParentMId))
                {
                    break;
                }
             //   index++;
                TempRid= GetRidByMid(ParentMId);
                if (TempRid == 0) { break; }
                ParentRId = TempRid;
                ParentMId = GetMIdByRId(ParentRId);
            }
            //for (int i = 0; i < ArrayOfTopMemebers.Length; i++)
            //{
            //    int currentMember = ArrayOfTopMemebers[i];
            //    GetNoOfKitsByMId(currentMember);
            //}
            Level1MId = ParentMId;
            int LvlOfRefdMid = GetLvlIdByMId(RefdMId);
            int LvlPerOfRefd = GetLvlPerByLvlId(LvlOfRefdMid);
            int TotalInc = NoOfKits * 1500 * 20 / 100;
            int RefdInc = NoOfKits * 1500 * LvlPerOfRefd / 100;
            int Level_1Inc = TotalInc - RefdInc;
            InsertIncInLevel1MId(Level1MId, Level_1Inc);
            InsertIncInRefdMId(RefdInc, RefdMId);
            InsertIncInSIT(Level1MId, RefdMId, Level_1Inc);
            int RefdRId = GetMyRidByMid(RefdMId); //GetRidByMid(RefdMId);
            int RefdMid_NoOfKits = GetAll_NoOfKits_ByRefdId(RefdRId);
            int RefdOwnKits = GetNoOfKitsByMId(RefdMId);
            int Refd_Total_Kits = RefdMid_NoOfKits + RefdOwnKits;
            int Promotion_Level = GetLvlIdByRefdTotalKits(Refd_Total_Kits);
            PromotRefd_Level(RefdMId, Promotion_Level);

        }

        private int GetMidByUserName(string newUserName)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(m => m.UserName == newUserName);
                if (user != null)
                {
                    return user.MemberId;
                }
                return 0;
            }
            catch (Exception)
            {

                throw;
            }
        }

        internal int RefCodeToMId(string refCode)
        {
            try
            {
                var user = CMDBE.tblReferralCodes.FirstOrDefault(x => x.ReferralCode == refCode);
                if (user != null)
                {
                    return user.MemberId;
                }
                return 0;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void PromotRefd_Level(int refdMId, int promotion_Level)
        {
            try
            {
                tblMember tblm = CMDBE.tblMembers.FirstOrDefault(c => c.MemberId == refdMId);
                tblm.LevelId = promotion_Level;
                CMDBE.SaveChanges();
                tblReferralCode tblrc = CMDBE.tblReferralCodes.FirstOrDefault(r => r.MemberId == refdMId);
                tblrc.LevelNo = promotion_Level;
                CMDBE.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private int GetLvlIdByRefdTotalKits(int refd_Total_Kits)
        {
            int LevelId = 0;
            int kits = refd_Total_Kits;
            if (kits <= 3) { LevelId = 6; }
            else if (kits <= 6) { LevelId = 5; }
            else if (kits <= 9) { LevelId = 4; }
            else if (kits <= 15) { LevelId = 3; }
            else if (kits <= 24) { LevelId = 2; }
            else  { LevelId = 1; }
            return LevelId;
        }

        private int GetAll_NoOfKits_ByRefdId(int refdRId)
        {
            try
            {
                List<tblMember> Users = CMDBE.tblMembers.ToList();
                if (Users != null)
                {
                    int TotalKits = 0;
                    foreach (var user in Users)
                    {
                        if (user.ReferralId==refdRId)
                        {
                            TotalKits += GetNoOfKitsByMId(user.MemberId);
                        }
                    }
                    return TotalKits;
                }
                return 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private int GetNoOfKitsByMId(int memberId)
        {
            try
            {
                var user = CMDBE.tblKits.FirstOrDefault(k => k.MemberId == memberId);
                if (user != null)
                {
                    int kits = Convert.ToInt32(user.NoOfKits);
                    return kits;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void InsertIncInSIT(int level1MId, int refdMId, int level_1Inc)
        {
            try
            {
                tblSIT tblsit = new tblSIT();
                tblsit.SuperMId = level1MId;
                tblsit.SalesMId = refdMId;
                tblsit.Incentive = level_1Inc;
                tblsit.CreatedAt = DateTime.Now.Date;
                tblsit.ModifiedAt = DateTime.Now.Date;
                CMDBE.tblSITs.Add(tblsit);
                CMDBE.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void InsertIncInRefdMId(int refdInc, int refdMId)
        {
            try
            {
                tblMember tblm = CMDBE.tblMembers.FirstOrDefault(s => s.MemberId == refdMId);
                if (tblm != null)
                {
                    tblm.Incentive = refdInc;
                    CMDBE.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void InsertIncInLevel1MId(int level1MId, int level_1Inc)
        {
            try
            {
                tblMember tblm = CMDBE.tblMembers.FirstOrDefault(s => s.MemberId == level1MId);
                if(tblm!=null)
                {
                    tblm.Incentive = level_1Inc;
                    CMDBE.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private int GetLvlPerByLvlId(int LevelId)
        {
            int Percent = 0;
            switch (LevelId)
            {
                case 1:
                    Percent = 20;
                    break;
                case 2:
                    Percent = 18;
                    break;
                case 3:
                    Percent = 16;
                    break;
                case 4:
                    Percent = 13;
                    break;
                case 5:
                    Percent = 10;
                    break;
                case 6:
                    Percent = 6;
                    break;
            }
            return Percent;
        }

        private int GetLvlIdByMId(int refdMId)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(z => z.MemberId==refdMId);
                if (user != null)
                {
                    return user.LevelId;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        internal int GetMIdByRId(int parentRId)
        {
            try
            {
                var user = CMDBE.tblReferralCodes.FirstOrDefault(x => x.ReferralId == parentRId);
                if (user != null)
                {
                    return user.MemberId;
                }
                return 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private int GetRidByMid(int mid)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(x => x.MemberId == mid);
                if (user != null)
                {
                    int rid = Convert.ToInt32(user.ReferralId);
                    return rid;
                }
                return 0;

            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool IsLevel1Person(int refdMId)
        {
            try
            {
                var user = CMDBE.tblMembers.FirstOrDefault(z => z.MemberId == refdMId && z.LevelId == 1);
                if (user != null)
                {
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}