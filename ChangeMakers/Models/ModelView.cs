﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChangeMakers.Models
{
    public class MemberViewModel
    {
        public int MemberId { get; set; }
        [Required(ErrorMessage ="UserName is Required.",AllowEmptyStrings =false)]
        public string UserName { get; set; }
        [Required(ErrorMessage = "First Name is Required.", AllowEmptyStrings = false)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last Name is Required.", AllowEmptyStrings = false)]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Email is Required.", AllowEmptyStrings = false)]
        [RegularExpression(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z",
        ErrorMessage = "Please provide valid email id")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Mobile No is Required.", AllowEmptyStrings = false)]
        public Nullable<long> MobileNo { get; set; }
        [Required(ErrorMessage = "Password is Required.", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [RegularExpression("([a-z]|[A-Z]|[0-9]|[\\W]){4}[a-zA-Z0-9\\W]{3,11}",ErrorMessage ="Enter Standerd Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfPass { get; set; }
        [Required]
        public int LevelId { get; set; }
        public Nullable<int> ReferralId { get; set; }
        public string ReferralCode { get; set; }
        [Required(ErrorMessage ="Please check tearm and conditons")]
        [Range(typeof(bool), "true","true", ErrorMessage = "Please check tearm and conditons!")]
        public bool CheckTerm { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public bool IsActivated { get; set; }
        public Nullable<long> AdvanceBalance { get; set; }
        public Nullable<long> DueBalance { get; set; }
        public Nullable<double> CryptoAmt { get; set; }
        public string CryptoType { get; set; }
    }
    public class LoginViewModel
    {
        [Required(ErrorMessage = "User Name is Required", AllowEmptyStrings = false)]
        [Display(Name = "User Name: ")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is Required", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Display(Name = "Password: ")]
        public string Password { get; set; }
    }
    public class VerifyViewModel
    {
        public int MemberId { get; set; }
        [Required(ErrorMessage = "Please Enter Verification Code")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Enter Verification Code: ")]
        public string EnterVerificationCode { get; set; }
    }
    public class KYCViewModel
    {
        public int MemberId { get; set; }
        public string PanNo { get; set; }
        public string PanImage { get; set; }
        public int PINCode { get; set; }
        public string State { get; set; }
        public string AddressProof { get; set; }
        public string ProofNo { get; set; }
        public string ProofFront { get; set; }
        public string ProofBack { get; set; }
    }
    public class ReferralVeiwModel
    {
        public string Rcodestr { get; set; }
        public string Datestr { get; set; }
        public string RefdName { get; set; }
    }
    public class MyMemebersViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Level { get; set; }
        public int Incentive { get; set; }
    }
}