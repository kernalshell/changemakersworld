﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mail;
using System.Net;
using System.Web.Helpers;
using ChangeMakers.Controllers;

using ChangeMakers.Models;
using System.Runtime.InteropServices;
using System.IO;
using Amazon.S3;
using Amazon;
using Amazon.S3.Transfer;
using System.Security.Cryptography;
using System.Text;

namespace ChangeMakers.AccessClasses
{
    public class ReqResp
    {
        public bool SendVerificationMail(string email)
        {
            string ActivationUrl = "";
            try
            {
                //      ActivationUrl = Server.HtmlEncode("http://localhost:54165/Home/Verification?email=" + email + "&key=" + GetKey(email));
             //   ActivationUrl = server.HtmlEncode("http://changemakersworld-env.8rtq3qmmqv.us-east-2.elasticbeanstalk.com/Home/Verification?email=" + email + "&key=" + GetKey(email));

                WebMail.Send(email,
                          "Change Makers World-Verify Your Email",
                          "Welcome  <br/>" + "<a class='label' href='" + ActivationUrl + "'>Click Here to verify your acount</a>" + "<br/>Thanks, <br/><b>Team Changemakesworld</b>",
                          null, null, null,
                          true,
                          null, null, null, null, null,
                          email
                          );
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        void date()
        {
            var cdate = DateTime.Now;
            var edate = HttpUtility.UrlEncode(cdate.ToString());
            var fdate = HttpUtility.UrlDecode(edate);
            var date = Convert.ToDateTime(fdate);
        }


        public bool UploadImage(List<HttpPostedFileBase> files,string username)
        {
            bool a = false;
            for (int i = 0; i <files.Count; i++)
            {
                string name = string.Empty;
                Stream st = files[i].InputStream;
                if(i==0)
                {
                    name = username + "PANImage";
                }
                else if(i==1)
                {
                     name = username + "proofFrontSide";
                }
                else 
                {
                     name = username + "ProofBackSide";
                }
                string myBucketName = "cmwkyc"; //your s3 bucket name goes here  
                string s3DirectoryName = "kycpic";
                string s3FileName = name;

                a = sendMyFileToS3(st, myBucketName, s3DirectoryName, s3FileName);

            }
            
            if (a == true)
            {
                return true;

            }
            else
                return false;
        }

        public bool UploadProfilePic(HttpPostedFileBase userprofilepic, string user)
        {
            bool a = false;
                string name = user+"ProfilePic";
                Stream st = userprofilepic.InputStream;
                string myBucketName = "cmwkyc"; //your s3 bucket name goes here  
                string s3DirectoryName = "profilepic";
                string s3FileName = name;
                a = sendMyFileToS3(st, myBucketName, s3DirectoryName, s3FileName);
            if (a == true)
            {
                return true;

            }
            else
                return false;
        }
        private bool sendMyFileToS3(System.IO.Stream localFilePath, string bucketName, string subDirectoryInBucket, string fileNameInS3)
        {
            IAmazonS3 client = new AmazonS3Client(RegionEndpoint.USEast2);
            TransferUtility utility = new TransferUtility(client);

            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();

            if (subDirectoryInBucket == "" || subDirectoryInBucket == null)
            {
                request.BucketName = bucketName; //no subdirectory just bucket name  
            }
            else
            {   // subdirectory and bucket name  
                // request.BucketName = bucketName + @"/" + subDirectoryInBucket;
                // request.BucketName = bucketName+"/" + subDirectoryInBucket;
                request.BucketName = bucketName + @"/"+subDirectoryInBucket;

            }
            request.Key = fileNameInS3; //file name up in S3
            request.CannedACL = S3CannedACL.PublicRead;
            request.InputStream = localFilePath;
            utility.Upload(request); //commensing the transfer  

            return true; //indicate that the file was sent  
        }

        public string Encrypt(string clearText)
        {
            string EncryptionKey = "abc123";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "abc123";
            cipherText = cipherText.Replace(" ", "+");
            int mod4 = cipherText.Length % 4;
            if (mod4 > 0)
            {
                cipherText += new string('=', 4 - mod4);
            }
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
}