﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChangeMakers.ViewModels
{
    public class SignUpViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public Nullable<long> MobileNo { get; set; }
        [Required]
        public string Password { get; set; }
        public string ConfPass { get; set; }
        [Required]
        public string NoOfKits { get; set; }
        public string ReferralCode { get; set; }
        [Required]
        public bool CheckTerm { get; set; }

    }
}