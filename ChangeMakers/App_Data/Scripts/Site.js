﻿AdminListShow();
function AdminListShow() {
    $('#LoadingUserStatus').html("Loading....");
    $.get("/Admin/GetUserList", null, UserDatabind);
    function UserDatabind(AdminList) {
        debugger;
        var setData = $("#SetUserList");
        if (AdminList.length >= 1) {
            for (var i = 0; i < AdminList.length; i++) {
                var UserData = "<tr class='remove_row" + AdminList[i].MemberId + "'>" +
                    "<td>" + AdminList[i].UserName + "</td>" +
                    "<td>" + AdminList[i].FirstName + "</td>" +
                    "<td>" + AdminList[i].LastName + "</td>" +
                    "<td>" + AdminList[i].Email + "</td>" +
                    "<td>" + AdminList[i].MobileNo + "</td>" +
                    "<td>" + AdminList[i].LevelId + "</td>" +
                    "<td>" + AdminList[i].ReferralId+ "</td>" +
                    "<td>" + ConvertJsonDateString(AdminList[i].CreatedAt) + "</td>" +
                    "<td>" + ConvertJsonDateString(AdminList[i].ModifiedAt) + "</td>" +
                    "<td>" + BoolToYesNo( AdminList[i].IsActivated)+ "</td>" +
                    "<td>" + AdminList[i].AdvanceBalance + "</td>" +
                    "<td>" + AdminList[i].DueBalance + "</td>" +
                    "<td>" + AdminList[i].CryptoAmt + "</td>" +
                    "<td>" + AdminList[i].CryptoType + "</td>" +
                    "<td>" + "<a href='#' class='btn btn-warning' onclick='EditUserRecord(" + AdminList[i].MemberId + ")' data-toggle='modal' data-target='#AddmyUserModal'><span class='glyphicon glyphicon-edit'></span></a>" + "</td>" +
                    "<td>" + "<a href='#' class='btn btn-danger' onclick='DeleteUserIdRecord(" + AdminList[i].MemberId + ")' data-toggle='modal' data-target='#DeleteUserConfirmation'><span class='glyphicon glyphicon-trash'></span></a>" + "</td>" +
                    "</tr>";
                debugger;
                setData.append(UserData);
                $('#LodingUserStatus').html("");
            }
        } else {
            var NoData = "<tr>" + "<td>" + "</td> " + "<td style='color:red'>" + "<b>" + "No File Found" + "</b>" + "</td>" + "</tr>";
            setData.append(NoData);
        }
    }
}
function ConvertJsonDateString(jsonDate) {
    var shortDate = null;
    if (jsonDate) {
        var regex = /-?\d+/;
        var matches = regex.exec(jsonDate);
        var dt = new Date(parseInt(matches[0]));
        var month = dt.getMonth() + 1;
        var monthString = month > 9 ? month : '0' + month;
        var day = dt.getDate();
        var dayString = day > 9 ? day : '0' + day;
        var year = dt.getFullYear();
        shortDate = monthString + '-' + dayString + '-' + year;
    }
    return shortDate;
};
function BoolToYesNo(boolvalue) {
    if(boolvalue=='true') {
        return "Yes";
    }
    else
        return "No";
};
var DefaulId = 0;
$('#AddNewUserRecordbtn').click(function () {
    $('#UserModalTitle').html("Add New Record");
    $('#SaveUserRecord').text("Save");
    $('#UserName').val("");
    $('#FirstName').val("");
    $('#LastName').val("");
    $('#Email').val("");
    $('#MobileNo').val("");
    $('#LevelId').find('option:selected').text("--Select No of Kits--");
    $('#ReferredBy').find('option:selected').text("--Select Referred By--");
    $('#ReferralId').find('option:selected').text("--Select ReferralId--");
    $('#CreatedAt').val('');
    $('#ModifiedAt').val('');
    $('#IsActivated').attr('checked', false);
    $('#AdvanceBalance').val("");
    $('#DueBalance').val("");
    $('#CryptoAmt').val("");
    $('#CryptoType').val("");
    DefaulId = 0;
})

$("#SaveUserRecord").click(function () {
    var Userdata = new Object();
    Userdata.UserName = $('#UserName').val();
    Userdata.FirstName = $('#FirstName').val();
    Userdata.LastName = $('#LastName').val();
    Userdata.Email = $('#Email').val();
    Userdata.MobileNo = $('#MobileNo').val();
    Userdata.Password = $('#Password').val();
    Userdata.ConfPass = $('#ConfPass').val();
    Userdata.LevelId = $('#LevelId').find('option:selected').text();
    Userdata.ReferredBy = $('#ReferredBy').find('option:selected').text();
    Userdata.ReferralId = $('#ReferralId').find('option:selected').text();
    Userdata.CreatedAt = $('#CreatedAt').val();
    Userdata.ModifiedAt = $('#ModifiedAt').val();
    if ($('#IsActivated').attr('checked'))
        Userdata.IsActivated = true;
    else
        Userdata.IsActivated = false;
    Userdata.AdvanceBalance = $('#AdvanceBalance').val();
    Userdata.DueBalance = $('#DueBalance').val();
    Userdata.CryptoAmt = $('#CryptoAmt').val();
    Userdata.CryptoType = $('#CryptoType').val();
    Userdata.MemberId = DefaultId;
    debugger;
    if (Userdata != null) {
        $.ajax({
            type: "POST",
            url: "/Admin/SaveUserDetails",
            data: JSON.stringify(Userdata),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (UserAddedResponce) {
                if (UserAddedResponce == true) {
                    alert("Record Added Successfully");
                    window.location.href = "/Home/UserloyeeMaster";
                    $("#AddmyUserModal").modal("hide");
                } else {
                    alert("Please  Correct Enter Details");
                }
            },
            failure: function (UserAddedResponce) {
                alert(UserAddedResponce.responseText);
            },
            error: function (UserAddedResponce) {
                alert(UserAddedResponce.responseText);
            }
        });
    }
});

function EditUserRecord(UserId) {
    $("#UserModalTitle").html("Update Record");
    $('#SaveUserRecord').text("Update");
    var url = "/Admin/GetUserById";
    DefaultId = UserId;
    $.ajax({
        type: "GET",
        data: { 'UserId': UserId},
        url: url,
        success: function (eidwithdata) {
            debugger;
            var obj = JSON.parse(eidwithdata);
            $('#UserName').val(obj.UserName);
            $('#FirstName').val(obj.FirstName);
            $('#LastName').val(obj.LastName);
            $('#Email').val(obj.Email);
            $('#MobileNo').val(obj.MobileNo);
            $('#Password').val(obj.Password);
            $('#LevelId').find('option:selected').text(obj.LevelId);
           // $('#ReferredBy').find('option:selected').text(obj.ReferralId);
            $('#ReferralId').find('option:selected').text(obj.ReferralId);
            $('#CreatedAt').text(obj.CreatedAt);
            $('#ModifiedAt').text(obj.ModifiedAt);
            $('#IsActivated').attr('checked', obj.IsActivated);
            $('#AdvanceBalance').val(obj.AdvanceBalance);
            $('#DueBalance').val(obj.DueBalance);
            $('#CryptoAmt').val(obj.CryptoAmt);
            $('#CryptoType').val(obj.CryptoType);
        }
    });
}

function DeleteUserIdRecord(dUserId) {
    $('#ConfirmUserDelete').click(function () {
        debugger;
        alert(dUserId);
        $.ajax({
            type: "GET",
            url: "/Admin/DeleteMemeber?DUserid=" + dUserId,
            success: function (DeleteUserResult) {
                if (DeleteUserResult) {
                    $(".remove_row" + dUserId).remove();
                    $("#DeleteUserConfirmation").modal("hide");
                }
            }
        });
    });
}




