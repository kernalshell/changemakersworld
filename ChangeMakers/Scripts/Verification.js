﻿$('#verifymyaccount').click(function () {
    $('forvalidation').remove();
    if(IsValid()){
        var email = $('#useremail').val();
        var url = "/Home/VerifyEmail";
        $.ajax({
            type: "POST",
            data: { 'Email': email},
            url: url,
            success: function (resultdata) {
                if (!resultdata) {
                    alert("Wrong Email.! Email does'n exist in database");
                } else {
                    window.location.assign("/Error/Activation.html");
                }
            }
        });
    }
});
function IsValid() {
    var email = $('#useremail').val();
    if (email=="") {
        $('#infolbl').remove();
        var message = "This Field is Required";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:red'></label>").text(message);
        $('#foremail').append(infolabel);
        return false;
    }
    var epattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!email.match(epattern)) {
        $('#infolbl').remove();
        var message = "Enter Valid Email";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:red'></label>").text(message);
        $('#foremail').append(infolabel);
        return false;
    }
    $('#infolbl').remove();
    return true;
}
var labelwarn = "";
$('#NewPassword').change(function () {
    var pass = $('#NewPassword').val();
    if (pass == null) {
        $('#infolbl').remove();
        var message = "This field is required";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:#c21919e6'></label>").text(message);
        $('#fornew').append(infolabel);
    }
    var epattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/;
    if (!pass.match(epattern)) {
        $('#infolbl').remove();
        var message = "Enter Valid Password including alphanumeric and special symbol!";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:#c21919e6'></label>").text(message);
        $('#fornew').append(infolabel);
    } else {
        $('#infolbl').remove();
    }
});
$('#ConfPassword').change(function () {
    var pass = $('#NewPassword').val();
    var confpass = $('#ConfPassword').val();
    if (pass != confpass) {
        $('#infolbl').remove();
        var message = "Password and Confirm Password do not match!";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:#c21919e6'></label>").text(message);
        $('#forconf').append(infolabel);
    }
});


$('#btnResetPassword').click(function () {
    if (IsValidPass()) {
        swal({
            title: 'Please Wait',
            onOpen: () => {
                swal.showLoading()
            }
        })
        var email = $('#useremail').val();
        var pass = $('#NewPassword').val();
        axios.post('/User/ResetPass', {
            email: email,
            pass: pass
        }).then(function (response) {
            debugger;
            if (response.data) {
                swal("Password Updated Status", "Your Password has been Updated Successfully", "success", { allowOutsideClick: false });
                    window.location.href = "/Home/Login";
            } else {
                swal("Password Updated Status", "There is some thing wrong please try again later! ", "error");
            }
                console.log(response);
            }).catch(function (error) {
                console.log(error);
            });
    }
});
function IsValidPass() {
    debugger;
    var pass = $('#NewPassword').val();
    if (pass == null) {
        $('#infolbl').remove();
        var message = "This field is required";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:#c21919e6'></label>").text(message);
        $('#fornew').append(infolabel);
        return false;
    }
    $('#infolbl').remove();
    var epattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/;
    if (!pass.match(epattern)) {
        $('#infolbl').remove();
        var message = "Enter Valid Password including alphanumeric and special symbol!";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:#c21919e6'></label>").text(message);
        $('#fornew').append(infolabel);
        return false;
    }
    var cpass = $('#ConfPassword').val();
    if (cpass != pass) {
        $('#infolbl').remove();
        var message = "Confirm password do'n match with new password!";
        var infolabel = $("<label id='infolbl' style='font-size:small;color:#c21919e6'></label>").text(message);
        $('#forconf').append(infolabel);
        return false;
    }
    $('#infolbl').remove();
    return true;
}

$('.input100').focus(function () {
    $('#infolbl').remove();
});
$('#forgotbtn').click(() => {
    //swal({
    //    title: 'Submit email to run ajax request',
    //    input: 'email',
    //    showCancelButton: true,
    //    confirmButtonText: 'Submit',
    //    showLoaderOnConfirm: true,
    //    preConfirm: function (email) {
    //        return new Promise(function (resolve, reject) {
    //            setTimeout(function () {
    //                if (email === 'taken@example.com') {
    //                    reject('This email is already taken.')
    //                } else {
    //                    resolve()
    //                }
    //            }, 2000)
    //        })
    //    },
    //    allowOutsideClick: false
    //}).then(function (email) {
    //    swal({
    //        type: 'success',
    //        title: 'Ajax request finished!',
    //        html: 'Submitted email: ' + email
    //    })
    //}).catch(swal.noop)
});

$('#Loginbtn').click(() => {
    if (Isvalidlogin()) {
        swal({
            title: 'Please Wait',
            onOpen: () => {
                swal.showLoading()
            }
        })
        var logindata = new Object();
        logindata.UserName = $('#username').val();
        logindata.Password = $('#userpass').val();
        debugger;
        //axios.post('/Home/Login12', logindata).then(function (response) {
        //    debugger;
        //    if (response.data=="(1)") {
        //        window.location.href = "/Home/Index";
        //    }
        //    if (response.data=="(2)") {
        //        swal("InActive User", "You have not verified your email account during registration,"+
        //            " click the following link to verify your account on login form!!", "warning");
        //        $('#ReVerify').show();
        //    }
        //    if (response.data == "(3)") {
        //        alert("kjrfk");
        //        swal("Invalid Details", "You have entered wrong username or password!", "error");
        //    }
        //    console.log(response);
        //}).catch(function (error) {
        //    console.log(error);
        //});
        //  var username = $('#username').val();
        //  var password = $('#userpass').val();
        ////  var model = { email: username, key: password };

        var url = "/Home/Login12";
        $.ajax({
            type: "POST",
            data: logindata,
            url: url,
            success: function (resultdata) {
                debugger;
                if (!resultdata) {
                    swal("Wrong Email", "Wrong Email.! Email does'n exist in database", "error");
                } else {
                    window.location.href = "/Home/Index";
                }
            }
        });

    }
});


//////////////////////////////////////////////       KYC           /////////////////////////////////////////////////


$('#btnsubmitkyc').click(() => {

    if (IsValidkycform()) {
        swal({
            title: 'Please Wait',
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        var data = new FormData();
        var pancardfile = $("#pancardfile").get(0).files;
        if (pancardfile.length > 0)
            data.append("Pancardfile", pancardfile[0]);
        var frontsidefile = $("#frontsidefile").get(0).files;
        if (frontsidefile.length > 0)
            data.append("Frontsidefile", frontsidefile[0]);
        var backsidefile = $("#backsidefile").get(0).files;
        if (backsidefile.length > 0)
            data.append("Backsidefile", backsidefile[0]);
        var PanNo = $('#kycpancardno').val();
        var PINCode = $('#kycpin').val();
        var State = $('#kycstate').val();
        var AddressProof = $('#kycaddrproof').val();
        var ProofNo = $('#kycaadharno').val();
        data.append("PanNo", PanNo); data.append("PINCode", PINCode);
        data.append("State", State); data.append("AddressProof", AddressProof);
        data.append("ProofNo", ProofNo);
        $.ajax({
            url: "/User/UploadFile",
            type: "POST",
            processData: false,
            contentType: false,
            data: data,
            success: function (response) {
                if (response == "KYC is Ok") {
                    debugger;
                    swal("KYC Completed", "Your KYC is already Completed successfully.", "success", {
                    }).then(() => {
                        window.location.href = "/Home/Index";
                    });
                }
                else if (response) {
                    swal("KYC Completed", "Your KYC completed successfully.", "info", {
                    }).then(()=>{
                        window.location.href = "/Home/Index";
                    });
                } else {
                    swal("Fail to Update KYC", "Seeion time out, Please Login again to continue KYC","error");
                }
            },
            error: function (er) {
                swal(er);
            }

        });
    }
});
//////////////// KYC /////

$('#kycpancardno').change(() => {
    debugger;
    var This = $('#kycpancardno');
    var panno = $('#kycpancardno').val();
    if (panno.length != 10) {
        _Alert(This);
    }
});
$('#kycpancardno').focus(() => {
    $('#kycpancardno').css("border-color", "#d9dada");
});
$('#kycpancardno').change(() => {
    debugger;
    var This = $('#kycpancardno');
    var panno = $('#kycpancardno').val();
    if (panno.length != 10) {
        _Alert(This);
    } else {
        _Resume(This);
    }
});
$('#kycpancardno').change(() => {
    debugger;
    var This = $('#kycpancardno');
    var panno = $('#kycpancardno').val();
    if (panno.length != 10) {
        _Alert(This);
    } else {
        _Resume(This);
    }
});
$('#kycpin').change(() => {
    debugger;
    var This = $('#kycpin');
    var panno = $('#kycpin').val();
    if (panno.length != 6) {
        _Alert(This);
    } else {
        _Resume(This);
    }
});
$('#kycaadharno').change(() => {
    debugger;
    var This = $('#kycaadharno');
    var panno = $('#kycaadharno').val();
    if (panno==(null||"")) {
        _Alert(This);
    } else {
        _Resume(This);
    }
});
$('#kycaadharno').attr('disabled', true);
$('#kycaddrproof').change(() => {
    debugger
    $('#kycaadharno').attr('disabled', false);
    var selectedval = $('#kycaddrproof').val();
    if (selectedval == "---Select Address Proof---") {
        $('#kycaadharno').val('');
        $('#kycaadharno').attr('placeholder', 'Invalid Selection');
        $('#kycaadharno').attr('disabled', true);
    } else {
        $('#kycaadharno').val('');
        $('#kycaadharno').attr('placeholder', 'Enter ' + selectedval + " No");
    }
});
function _Alert(This) {
    This.css("border-color", "#fd4343");
}
function _Resume(This) {
    This.css("border-color", "#d9dada");
}

/////////////////////////
function IsValidkycform() {
    debugger;
    var kycpancardno = $('#kycpancardno').val();
    var panphoto = $('#pancardfile').val();
    var kycpin=$('#kycpin').val();
    var kycstate = $('#kycstate').val();
    var kycaddrproof = $('#kycaddrproof').val();
    var kycaadharno = $('#kycaadharno').val();
    var frontsidefile = $('#frontsidefile').val();
    var backsidefile = $('#backsidefile').val();
    if (kycpancardno == (null || "")) {
        _Alert($('#kycpancardno'));
        return false;
    }
    if (kycpancardno != (null || "")) {
        if (kycpancardno.length != 10) {
            _Alert($('#kycpancardno'));
            return false;
        } else {
            var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
            if (!regpan.test(kycpancardno)) {
                _Alert($('#kycpancardno'));
                return false;
            }
        }
    } else {
        _Alert($('#kycpancardno'));
        return false;
    }
    if (kycpin != (null || "")) {
        if (kycpin.length != 6) {
            return false;
        }
    } else {
        _Alert($('#kycpin'));
        return false;
    }
    if (kycaadharno == (null || "")) {
        _Alert($('#kycaadharno'));
        return false;
    }
    if (kycstate == "---Select State---") {
        result = false;
    }
    if (kycaddrproof == "---Select Address Proof---") {
        result = false;
    }
    if (!IsValidImage(panphoto)) {
        _Alert($('#pancardfile'));
        return false;
    }

    if (!IsValidImage(frontsidefile)) {
        return false;
    }
    if (!IsValidImage(backsidefile)) {
        return false;
    }
    return true;
}
function IsValidImage(sFileName) {
    debugger;
    var _validFileExtensions = [".jpg", ".jpeg", ".gif", ".png"];
    if (sFileName.length > 0) {
        var blnValid = false;
        for (var j = 0; j < _validFileExtensions.length; j++) {
            var sCurExtension = _validFileExtensions[j];
            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                blnValid = true;
                break;
            }
        }
        if (!blnValid) {
            swal("Sorry, " + sFileName + " is Invalid"," allowed extensions are: " + _validFileExtensions.join(", "));
            return false;
        } else {
            return true;
        }
    }
    return false;
}
