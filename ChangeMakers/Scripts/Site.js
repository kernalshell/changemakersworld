﻿
var UserDataList = new Object();
AdminListShow();
function AdminListShow() {
    debugger;
    $('#LoadingUserStatus').html("Loading....");
    $.get("/Admin/GetUserList", null, UserDatabind);
    function UserDatabind(AdminList) {
        debugger;
        UserDataList = AdminList;
        var setData = $("#SetUserList");
        if (AdminList.length >= 1) {
            for (var i = 0; i < AdminList.length; i++) {
                var UserData = "<tr class='remove_row" + AdminList[i].MemberId + "'>" +
                    "<td>" + AdminList[i].UserName + "</td>" +
                    "<td>" + AdminList[i].FirstName + "</td>" +
                    "<td>" + AdminList[i].LastName + "</td>" +
                    "<td>" + AdminList[i].Email + "</td>" +
                    "<td>" + AdminList[i].MobileNo + "</td>" +
                    "<td>" + AdminList[i].LevelId + "</td>" +
                    "<td>" + AdminList[i].ReferralId+ "</td>" +
                    "<td>" + ConvertJsonDateString(AdminList[i].CreatedAt) + "</td>" +
                    "<td>" + ConvertJsonDateString(AdminList[i].ModifiedAt) + "</td>" +
                    "<td>" + BoolToYesNo( AdminList[i].IsActivated)+ "</td>" +
                    "<td>" + AdminList[i].AdvanceBalance + "</td>" +
                    "<td>" + AdminList[i].DueBalance + "</td>" +
                    "<td>" + AdminList[i].CryptoAmt + "</td>" +
                    "<td>" + AdminList[i].CryptoType + "</td>" +
                    "<td>" + "<a href='#' class='btn btn-warning' onclick='EditUserRecord(" + AdminList[i].MemberId + ")' data-toggle='modal' data-target='#AddmyUserModal'><span class='glyphicon glyphicon-edit'></span></a>" + "</td>" +
                    "<td>" + "<a href='#' class='btn btn-danger' onclick='DeleteUserIdRecord(" + AdminList[i].MemberId + ")' data-toggle='modal' data-target='#DeleteUserConfirmation'><span class='glyphicon glyphicon-trash'></span></a>" + "</td>" +
                    "</tr>";
                setData.append(UserData);
               
            }
        } else {
            var NoData = "<tr>" + "<td>" + "</td> " + "<td style='color:red'>" + "<b>" + "No File Found" + "</b>" + "</td>" + "</tr>";
            setData.append(NoData);
        }
    }
}


function ConvertJsonDateString(jsonDate) {
    var shortDate = null;
    if (jsonDate) {
        var regex = /-?\d+/;
        var matches = regex.exec(jsonDate);
        var dt = new Date(parseInt(matches[0]));
        var month = dt.getMonth() + 1;
        var monthString = month > 9 ? month : '0' + month;
        var day = dt.getDate();
        var dayString = day > 9 ? day : '0' + day;
        var year = dt.getFullYear();
        shortDate = monthString + '-' + dayString + '-' + year;
    }
    return shortDate;
};
function BoolToYesNo(boolvalue) {
    if(boolvalue=='true') {
        return "Yes";
    }
    else
        return "No";
};

var DefaulId = 0;
$('#AddNewUserRecordbtn').click(function () {
    $('#UserModalTitle').html("Add New Record");
    $('#SaveUserRecord').text("Save");
    $('#UserName').val("");
    $('#FirstName').val("");
    $('#LastName').val("");
    $('#Email').val("");
    $('#MobileNo').val("");
    $('#LevelId').find('option:selected').text("--Select No of Kits--");
    $('#ReferredBy').find('option:selected').text("--Select Referred By--");
    $('#ReferralId').find('option:selected').text("--Select ReferralId--");
    $('#CreatedAt').val('');
    $('#ModifiedAt').val('');
    $('#IsActivated').attr('checked', false);
    $('#AdvanceBalance').val("");
    $('#DueBalance').val("");
    $('#CryptoAmt').val("");
    $('#CryptoType').val("");
    DefaulId = 0;
})

$("#SaveUserRecord").click(function () {
    var Userdata = new Object();
    Userdata.UserName = $('#UserName').val();
    Userdata.FirstName = $('#FirstName').val();
    Userdata.LastName = $('#LastName').val();
    Userdata.Email = $('#Email').val();
    Userdata.MobileNo = $('#MobileNo').val();
    Userdata.Password = $('#Password').val();
    Userdata.ConfPass = $('#ConfPass').val();
    Userdata.LevelId = $('#LevelId').find('option:selected').text();
    Userdata.ReferredBy = $('#ReferredBy').find('option:selected').text();
    Userdata.ReferralId = $('#ReferralId').find('option:selected').text();
    Userdata.CreatedAt = $('#CreatedAt').val();
    Userdata.ModifiedAt = $('#ModifiedAt').val();
    if ($('#IsActivated').attr('checked'))
        Userdata.IsActivated = true;
    else
        Userdata.IsActivated = false;
    Userdata.AdvanceBalance = $('#AdvanceBalance').val();
    Userdata.DueBalance = $('#DueBalance').val();
    Userdata.CryptoAmt = $('#CryptoAmt').val();
    Userdata.CryptoType = $('#CryptoType').val();
    Userdata.MemberId = DefaultId;
    debugger;
    if (Userdata != null) {
        $.ajax({
            type: "POST",
            url: "/Admin/SaveUserDetails",
            data: JSON.stringify(Userdata),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (UserAddedResponce) {
                if (UserAddedResponce == true) {
                    alert("Record Added Successfully");
                    window.location.href = "/Home/UserloyeeMaster";
                    $("#AddmyUserModal").modal("hide");
                } else {
                    alert("Please  Correct Enter Details");
                }
            },
            failure: function (UserAddedResponce) {
                alert(UserAddedResponce.responseText);
            },
            error: function (UserAddedResponce) {
                alert(UserAddedResponce.responseText);
            }
        });
    }
});

function EditUserRecord(UserId) {
    $("#UserModalTitle").html("Update Record");
    $('#SaveUserRecord').text("Update");
    var url = "/Admin/GetUserById";
    DefaultId = UserId;
    $.ajax({
        type: "GET",
        data: { 'UserId': UserId},
        url: url,
        success: function (eidwithdata) {
            debugger;
            var obj = JSON.parse(eidwithdata);
            $('#UserName').val(obj.UserName);
            $('#FirstName').val(obj.FirstName);
            $('#LastName').val(obj.LastName);
            $('#Email').val(obj.Email);
            $('#MobileNo').val(obj.MobileNo);
            $('#Password').val(obj.Password);
            $('#LevelId').find('option:selected').text(obj.LevelId);
           // $('#ReferredBy').find('option:selected').text(obj.ReferralId);
            $('#ReferralId').find('option:selected').text(obj.ReferralId);
            $('#CreatedAt').text(obj.CreatedAt);
            $('#ModifiedAt').text(obj.ModifiedAt);
            $('#IsActivated').attr('checked', obj.IsActivated);
            $('#AdvanceBalance').val(obj.AdvanceBalance);
            $('#DueBalance').val(obj.DueBalance);
            $('#CryptoAmt').val(obj.CryptoAmt);
            $('#CryptoType').val(obj.CryptoType);
        }
    });
}

function DeleteUserIdRecord(dUserId) {
    $('#ConfirmUserDelete').click(function () {
        debugger;
        $.ajax({
            type: "GET",
            url: "/Admin/DeleteMemeber?DUserid=" + dUserId,
            success: function (DeleteUserResult) {
                if (DeleteUserResult) {
                    $(".remove_row" + dUserId).remove();
                    $("#DeleteUserConfirmation").modal("hide");
                }
            }
        });
    });
}


////////////////////////////////////////////////////  Level Tree   //////////////////////////////////


var child = $('#parant').html();
$('#child').remove();
$('#Level_1').click(function () {
    debugger;
    SetDataOfThisLevel(1);
});
$('#Level_2').click(function () {
    SetDataOfThisLevel(2);
});
$('#Level_3').click(function () {
    SetDataOfThisLevel(3);
});
$('#Level_4').click(function () {
    SetDataOfThisLevel(4);
});
$('#Level_5').click(function () {
    SetDataOfThisLevel(5);
});
$('#Level_6').click(function () {
    SetDataOfThisLevel(6);
});
function SetDataOfThisLevel(lblid) {
    $('bootstrap-data-table').empty()
    $('#child').remove();
    $('#nodata').remove();
    $('#parant').append(child);
    var result = new Boolean();
    result = false;
    for (var i = 0; i < UserDataList.length; i++) {
        if (UserDataList[i].LevelId == lblid) {
            var setData = $('#tablebody');
            var UserData = "<tr class='remove_row" + UserDataList[i].MemberId + "'>" +
                "<td>" + UserDataList[i].UserName + "</td>" +
                "<td>" + UserDataList[i].FirstName + " " + UserDataList[i].LastName + "</td>" +
                "<td>" + UserDataList[i].MobileNo + "</td>" + "<td>" + UserDataList[i].Email + "</td>" +
                "</tr>";
            debugger;
            setData.append(UserData);
            result = true;
        }
    }
    if (!result) {
        $('#child').remove();
        $('#nodata').remove();
        var info = "<label id='nodata' class='label label-danger d-lg-block'>No Data Found</label>";
        $('#parant').append(info);
    }
}


///////////////////////////////////////////////////    Profile    //////////////////////////////////


var mid = $('#HiddenMemberId').val();
var UserPass = "";
var userfirstname, userlastname, usermobilino;
ShowThat();
function ShowThat() {
    $.get("/Admin/GetUserList", null, UserDetailsBind);
    function UserDetailsBind(UserDataList1) {
        debugger;
        for (var i = 0; i < UserDataList1.length; i++) {
            if (UserDataList1[i].MemberId == mid) {
                UserPass = UserDataList1[i].Password;
                $('#PUserName').text(UserDataList1[i].UserName);
                userfirstname = UserDataList1[i].FirstName;
                $('#PFirstName').text(userfirstname);
                userlastname = UserDataList1[i].LastName;
                $('#PLastName').text(userlastname);
                $('#PEmail').text(UserDataList1[i].Email);
                usermobilino = UserDataList1[i].MobileNo;
                $('#PMobileNo').text(usermobilino);
                $('#PLevelId').text($('#HiddenKit').val());
                $('#PReferralCode').text($('#HiddenRefCode').val());
                $('#PCreatedAt').text(ConvertJsonDateString(UserDataList1[i].CreatedAt));
                $('#PModifiedAt').text(ConvertJsonDateString(UserDataList1[i].ModifiedAt));
                $('#PAdvanceBalance').text(UserDataList1[i].AdvanceBalance);
                $('#PDueBalance').text(UserDataList1[i].DueBalance);
                $('#PCryptoAmt').text(UserDataList1[i].CryptoAmt);
                $('#PCryptoType').text(UserDataList1[i].CryptoType);
            }
        }
    }
}
$('#btnuserUpdate').click(function () {
    debugger;
    $('#PEFirstName').val(userfirstname);
    $('#PELastName').val(userlastname);
    $('#PEMobileNo').val(usermobilino);
});
$('#btnchngpsw').click(function () {
    debugger;    
    $('#PEFirstName').val("");
    $('#PELastName').val("");
    $('#PEMobileNo').val("");
});
$('#btnUpdate').click(function () {
    debugger;
        if (IsValidEdit()) {
            var firstName = $('#PEFirstName').val();
            var lastName = $('#PELastName').val();
            var mobileNo = $('#PEMobileNo').val();
            var url = "/User/UpdateUserDetails";
            $.ajax({
                type: "POST",
                data: { 'MemberId': mid,'FirstName':firstName,'LastName':lastName,'MobileNo':mobileNo },
                url: url,
                success: function (resultdata) {
                    if (resultdata) {
                        debugger;
                        jQuery("#EditUserModal").modal('hide');
                        swal("Details Status", "Your details has been Updated Successfully!", "success", { timer: 5000 });
                        window.location.href = "/User/UserProfile";
                    } else {
                        alert("Fail To Update!");
                    }
               }
            });
        } 
});
$('#btnChangepswd').click(function () {
    debugger;
    if (IsValidChange()) {

        var pass = $('#PENewPassword').val();
        var url = "/User/UpdateUserPass";
        $.ajax({
            type: "GET",
            data: { 'MemberId': mid, 'Password': pass },
            url: url,
            success: function (resultdata) {
                if (resultdata) {
                    swal("Password Status", "Your password has been Updated Successfully!", "success", { timer: 5000 });
                   // alert("Details Updated Successfull");
                    jQuery("#ChangeUserModal").modal('hide');
                } else {
                    alert("Fail To Update");
                }
            }
        });
    }
});
function IsValidChange() {
    debugger;
    var oldpass = $('#PEOldPassword').val();
    var newpass = $('#PENewPassword').val();
    var rpass = $('#PERePassword').val();
    if (oldpass.trim() == (null || "")){
        var message="This field is Required";
        appendto = $('#foroldpass');
        ShowLabelMessage(message, appendto); 
        return false;
    }
    if (newpass.trim() == (null || "")) {
        var message = "This field is Required";
        appendto = $('#fornewpass');
        ShowLabelMessage(message, appendto);
        return false;

    }
    if (rpass.trim() == (null || "")) {
        var message = "This field is Required";
        appendto = $('#forrepass');
        ShowLabelMessage(message, appendto);
        return false;
    }
    var paswd = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,15}$/;
    if (!newpass.match(paswd)) {
        var message = "Make Strong password includin alphanumeric and special symbol";
        appendto = $('#fornewpass');
        ShowLabelMessage(message, appendto);
        return false;
    }
    if (newpass != rpass) {
        var message = "Password and confirm password Do Not Match";
        appendto = $('#forrepass');
        ShowLabelMessage(message, appendto);
        return false;
    }
    if (UserPass != oldpass) {
        var message = "Password Do Not Match with database";
        appendto = $('#foroldpass');
        ShowLabelMessage(message, appendto);
        return false;
    }
    if (newpass == oldpass) {
        var message = "Your New Password & Old Password is same..Try different!";
        appendto = $('#forrepass');
        ShowLabelMessage(message, appendto);
        return false;
    }
    return true;
}
function ShowLabelMessage(message, appendto) {
    $('#infolbl').remove();
    var infolabel = $("<label id='infolbl' style='font-size:small;color:red'></label>").text(message);
    appendto.append(infolabel);
}
function IsValidEdit() {
    var mobile = $('#PEMobileNo').val();
    if (mobile.length != 10) {
        var message = "Enter 10Digit Mobile No";
        var appendto = $('#formobileno');
        ShowLabelMessage(message, appendto);
        return false;
    }
    return true;
}

$('.UserMobileNo').keypress(function (e) {
    var k = e.which;
    var ok = (k >= 48 && k <= 57) ||  // 0-9
        k == 8 ||  // Backspaces
        k == 9 ||  //H Tab
        k == 11 ||  //V Tab
        k == 127;   //Delete

    if (!ok) {
        e.preventDefault();
    }
});


var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function (e) {
        var t = ""; var n, r, i, s, o, u, a; var f = 0; e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++); r = e.charCodeAt(f++); i = e.charCodeAt(f++); s = n >> 2; o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6; a = i & 63;
            if (isNaN(r)) { u = a = 64 } else if (isNaN(i)) { a = 64 } t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        } return t
    }, decode: function (e) {
        var t = ""; var n, r, i; var s, o, u, a; var f = 0; e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++)); o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++)); a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4; r = (o & 15) << 4 | u >> 2; i = (u & 3) << 6 | a; t = t + String.fromCharCode(n);
            if (u != 64) { t = t + String.fromCharCode(r) } if (a != 64) { t = t + String.fromCharCode(i) }
        } t = Base64._utf8_decode(t); return t
    }, _utf8_encode: function (e) {
        e = e.replace(/rn/g, "n"); var t = ""; for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) { t += String.fromCharCode(r) } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192); t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224); t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        } return t
    }, _utf8_decode: function (e) {
        var t = ""; var n = 0; var r = c1 = c2 = 0; while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) { t += String.fromCharCode(r); n++ } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63); n += 2
            } else {
                c2 = e.charCodeAt(n + 1); c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63); n += 3
            }
        } return t
    }
}
